![Photo of Nautilus by Manuae](Nautilus_Palau.JPG)

**DISCLAIMER**: These mats have been delivered 0 times. They are full of potholes and not all the bridges go all the way over. Drive carefully, learners. As always, patches R welcome!

# S is for Secure Shell

> What is Secure Shell?

Secure Shell is *the* way to get to the interactive CLI on a remote machine.

> Why is Secure Shell?

Telnet is terribly insecure. Serial cables are a pain in the ass. GUI connections are for the weak.

> When should you use Secure Shell?

Any time you don't actually need a GUI connection, and sometimes, when you do.

# Exercises

Ensure that you're using Linux, BSD, another unix, macOS, or ChromeOS correctly configured or Windows with CygWin/X or WSL correctly configured. "Correctly configured" means that you have ssh and an X Server that act a lot like they do on Linux. **Linux is recommended**, if for no better reason than this: At the time of this writing, this class has not been tested on anything else.

* [One](units/01/README.md): Of agents and their secret keys and ports
* [Two](units/02/README.md): Jump, tunnel, and shuttle
* [Three](units/03/README.md): Copy files around copying files with scp, sftp, rsync

# Credits
- [banner pic](https://commons.wikimedia.org/w/index.php?curid=18395466): Nautilus by Manuae, CC BY-SA 3.0
- [OpenSSH](https://www.openssh.com/)
- [rsync](https://rsync.samba.org/)
