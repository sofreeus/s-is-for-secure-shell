# S is for Secure Shell: Of secret keys, agents, and obscure ports

TODO - insert pic of spy

## Connect to a terminal server

The simplest operation to perform with ssh is to connect to another machine with a provided name and password. Do that now.

- hostname: darter.sofree.us
- username: osadmin
- password: (provided)

```bash
# straight ssh connection
ssh osadmin@darter.sofree.us
```

Great! While you're there, add a user account for yourself. You can use it for lots of later exercises, if you like.

```bash
add-learner $GITLAB_USERNAME
exit # or just [Control]+D
```

Test your personal login.

```bash
ssh $GITLAB_USERNAME@darter.sofree.us
exit # or just [Control]+D
```

If your local username doesn't match your GitLab username, add a stanza to your ssh config (~/.ssh/config) to override the username for this machine, like this.

```
Host darter.sofree.us
	User garheade
```

## Create a personal keypair

Typing passwords is inconvenient. Remembering them is difficult. Making up new ones all the time... Who has time for that? But seriously, use BitWarden or KeePass and use a unique password for every service. It's very important. If you have a password that you use in multiple places, use a trustworthy tool to search password lists for it, at least occasionally. I trust [have i been pwned: passwords](https://haveibeenpwned.com/Passwords) for that. Obviously, whenever one of your passwords appears in a cracking dictionary, change it everywhere you use it, and stop using it. Enough about passwords. Almost.

You'll put a password on your personal private key. Why? Isn't the key itself a huge, difficult-to-guess password, magically conjoined with a shareable bit (the public key)? No, but sure, but the point is, what if someone were to get their nasty claws on the device you store your personal private key on? Or, what if you stored your private key on something your sysadmin has access to? That person would be able to prove that they're you, to the satisfaction of GitLab.com and any servers you authorized the key on. They might do bad things in your name. So, you're going to encrypt your personal private key, so that Mr. or Ms. Nasty Claws has to spend a few years guessing about the password.

```bash
ssh-keygen # creates key files as ~/.ssh/id_rsa and ~/.ssh/id_rsa.pub by default
```

What if I forgot to encrypt my personal private key, or I want to change the password?

```bash
ssh-keygen -p -f some-private-key
```

## Authorize the local key pair for authentication

Now that you have a key pair, add the public key to authorized_keys on another machine, so that you can authenticate with your private key, and stop using a password.

```bash
ssh-copy-id darter.sofree.us
# a few non-interactive ssh commands
ssh darter.sofree.us uptime
ssh darter.sofree.us date

# interactively look at the changes to authorized_keys
ssh darter.sofree.us
ls -l ~/.ssh/
cat ~/.ssh/authorized_keys

exit # or just [Control]+D
```

For higher security, you might want to delete your password, or disable password authentication entirely. Don't do that now.

## Start an agent to keep your secret safe, more conveniently

"But, now I'm right back to typing passwords all the time!" No, you're not. You'll use ssh-agent to keep your key in memory for a session, while your personal private key remains safely encrypted on disk.

After you've tired of typing the password to decrypt your private key, start ssh-agent to remember it for you, for the duration of your session.

```bash
# This is pretty safe to add to your .bashrc or .zshrc
if ! [[ $SSH_AGENT_PID ]]; then
	echo "Starting a new SSH Agent"
	eval $( ssh-agent )
fi
ssh-add # to add the default key, you'll be prompted for decryption passphrase
ssh-add some-private-key # to add a particular key
ssh-add -l # show the keys that have been added to the agent
```

## Trusted and untrusted keys

Authorize your public key on GitLab or GitHub.

```bash
cat ~/.ssh/id_rsa.pub
```

Copy the whole thing into [your authorized keys on GitLab](https://gitlab.com/-/profile/keys) or GitHub.

If you plan to ssh to a machine using many different keys, perhaps because you have many different machines, each with its own key, and you carefully maintain your list of authorized keys on GitLab.com or GitHub.com, you and other sysadmins can use that list of keys to authenticate you.

Authorize here all the keys you have authorized on GitLab.com

```bash
# caution: This replaces the file content. If you want to append,
# you'll need to redirect with >> instead of > and maybe re-unique it.
curl https://gitlab.com/$GITLAB_USERNAME.keys > ~/.ssh/authorized_keys
# if authorized_keys did not pre-exist, you may need to tweak permissions
# chmod go-rwx ~/.ssh/authorized_keys
view ~/.ssh/authorized_keys
```

Now, you should be able to log in from any machine that has access to any private key matching any of the public keys you just authorized.

### Change the server port on a public ssh server

Why is it Port 22, by default? https://www.ssh.com/academy/ssh/port

If you look at the sshd log on any Internet-facing server running ssh on the default port, you'll see something like this:

```
Feb 04 23:12:56 blue.sofree.us sshd[7418]: Failed password for root from 112.85.42.227 port 60004 ssh2
Feb 04 23:12:56 blue.sofree.us sshd[7418]: pam_succeed_if(sshd:auth): requirement "uid >= 1000" not met by user "root"
Feb 04 23:12:58 blue.sofree.us sshd[7418]: Failed password for root from 112.85.42.227 port 60004 ssh2
Feb 04 23:12:59 blue.sofree.us sshd[7418]: Received disconnect from 112.85.42.227 port 60004:11:  [preauth]
Feb 04 23:12:59 blue.sofree.us sshd[7418]: Disconnected from 112.85.42.227 port 60004 [preauth]
Feb 04 23:12:59 blue.sofree.us sshd[7418]: PAM 2 more authentication failures; logname= uid=0 euid=0 tty=ssh ruser= rhost=112.85.42.227  user=root
Feb 04 23:13:00 blue.sofree.us sshd[7423]: pam_unix(sshd:auth): authentication failure; logname= uid=0 euid=0 tty=ssh ruser= rhost=112.85.42.227  user=root
Feb 04 23:13:00 blue.sofree.us sshd[7423]: pam_succeed_if(sshd:auth): requirement "uid >= 1000" not met by user "root"
Feb 04 23:13:02 blue.sofree.us sshd[7423]: Failed password for root from 112.85.42.227 port 56157 ssh2
Feb 04 23:13:02 blue.sofree.us sshd[7423]: pam_succeed_if(sshd:auth): requirement "uid >= 1000" not met by user "root"
```

If password auth is disabled and root auth is disabled, the bad guys are unlikely to get in, but still, you might like a less noisy log. In that case, a good thing you can do is change the server port.

1. Add the new listening port
2. Restart the service
2. Connect to the new port from a client
3. Remove the old listening port (and remove any obsolete firewall rules)

On the server side:

Ensure 'Port (number)' lines in `/etc/ssh/sshd_config`, like this:

```
Port 22 # un-comment this line, if it's commented
Port 22222 # add this line
```

And restart the service.

On the client side:

Connect to the new port ad hoc, like this:

```bash
ssh -o port=22222 darter.sofree.us
```

When that works, add the new port to your ssh client config, like this:

```
Host darter.sofree.us
	Port 22222
```

Now, you should be able to login without specifying the special port.

On the server, remove the default port from sshd_config and restart the service.

## Tunnel X

Sometimes, you just need a little GUI. And, wouldn't it be great if it could be really slow, like somewhere between the pace of a snail and glacier? This is for then.

On the up-side, it's quite secure.

This will probably just work if you're using Linux. If you're using something else, it might work, or you might be able to make it work by fiddling with it a bit. Feel free to borrow a laptop or a Raspberry Pi.

```bash
ssh -XY darter.sofree.us
xeyes &
xclock &
gedit &
firefox &
jobs
kill %2
jobs
```
