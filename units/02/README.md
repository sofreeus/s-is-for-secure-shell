# S is for Secure Shell: Jump, Tunnel, and Shuttle

TODO - Insert picture of rabbit in space suit

![network with layers](sifs-digo-network.jpg)

Sometimes, you can't directly reach the thing you want to reach, or for whatever reason, you want to obscure your actual IP.

## ProxyJump, when all you want is Secure Shell

ad hoc:

```bash
ssh -J [user@]jumphost [user@]remote-host
```

Set up your ssh config like this:

FIXME - This does not work. See https://therootcompany.com/blog/ssh-defaults-config-and-priorities/

```
User osadmin
Host sifs-lb
	HostName 157.245.244.136
Host sifs-1
	HostName 10.116.0.3
	ProxyJump sifs-lb
Host sifs-2
	HostName 10.116.0.4
	ProxyJump sifs-lb
Host sifs-3
	HostName 10.116.0.5
	ProxyJump sifs-lb
Host sifs-db
	HostName 10.116.0.6
	ProxyJump sifs-lb
```

A few things are interesting here:
- `User` isn't in a `Host` section, so it affects all hosts
- `HostName` can be an FQDN or an IP address, overriding DNS
- `ProxyJump` is the machine you ssh through

Later, we'll add user accounts to all the machines, then remove the user override line

For now, try this:

```bash
ssh sifs-db
psql postgres
create database $MACGUFFIN # where $MACGUFFIN is your favorite MacGuffin
```

## Tunnel a port, the old fashioned way

If you need to reach a non-public service or you want to obscure your source-ip while accessing some service, you can do so by using a traditional ssh tunnel where all the traffic directed to some `local-port` is sent to a `target-port` on a `target-host` by proxy through an `ssh-tunnel-host`. The client must have ssh access to the `ssh-tunnel-host` and the `ssh-tunnel-host` must have access to the `target-host:target-port`.

```bash
ssh -N -L local-port:target-host:target-port ssh-tunnel-host(waypoint)
```

```bash
curl http://ifconfig.me && echo # shows your true public ip
ssh -N -L 8000:ifconfig.me:80 darter.sofree.us
curl http://ifconfig.me && echo # still shows your true public ip
# but, if you send the query through the tunneled port, voila!
curl --header "Host: ifconfig.me" http://localhost:8000 && echo
```



My research of redirecting local-ports through ssh servers to ports on remote hosts led me to a similar capability where you map a port on the ssh server to a port on the local machine. Pretty cool.

```
ssh -R 8080:127.0.0.1:3000 -N -f user@remote.host
```

I've never done that one. It seems like it would be very useful for making otherwise invisible ssh, http, vnc, and rdp servers, visible. LMK how it works out if you try it.

## sshuttle - simple and fast

Sometime around the 7th port-forwards, you think, "This sucks." Maybe, you wonder if there's a better way... At that point, you should check out sshuttle. It's like adding all the tunnels at once, without the little host-header finagles, or it's like adding a route to a remote network, or it's like starting a VPN. The only weirdness is it's TCP only. I hardly notice. LMK if you do.

```bash
sshuttle --verbose --remote darter.sofree.us 192.168.73.0/24
```

Note that you can reach all the *.int.sofree.us now, like:
- https://pm.int.sofree.us:8006
- https://pve.int.sofree.us:8006
- https://pve-too.int.sofree.us:8006
- http://prometheus.int.sofree.us
- http://grafana.int.sofree.us
- http://alertmanager.int.sofree.us
- http://blackbox-exporter.int.sofree.us

Technically, if you're on SFS-WLAN or WillsonWLAN, you already could, but you can't reach these, no matter where you are:
- http://10.116.0.3
- http://10.116.0.4
- http://10.116.0.5

sshuttle through sifs-lb to get to those webservers

```bash
sshuttle --verbose --remote sifs-lb 10.116.0.0/20
```
