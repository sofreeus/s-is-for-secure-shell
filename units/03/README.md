# S is for Secure Shell: Copying files around

upload and run a simple script

Put some music on darter (as osadmin)
- scp
- sftp
- rsync

Copy down some files from darter
- scp
- sftp
- rsync

Note that scp and sftp always copy the files

rsync with -a copies only new/changed files

rsync with --delete removes orphans, files that exist on the dest, but not the source

when source and dest end with /, rsync syncs the directories, if the last directory of the dest doesn't exist, rsync will create it

[explainshell: rsync -avHP src/ dest/](https://explainshell.com/explain?cmd=rsync+-avHP+src%2F+dest%2F)
